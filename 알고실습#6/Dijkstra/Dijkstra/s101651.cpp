#include "Graph_include.h"

void Insertion(int* Heap, int* distance, int* Heap_Index, int Vertex_index, int size)
{
	Heap[++size] = Vertex_index;
	while((size != 1) && (distance[Vertex_index] < distance[Heap[size/2]])){
		Heap[size] = Heap[size/2];
		Heap_Index[Heap[size]] = size;
		size = size / 2;
	}//새로운 값이 삽입되는 위치 size를 찾으며, 조건에따라 기존 노드들을 이동시킨다.
	Heap[size] = Vertex_index;
	Heap_Index[Heap[size]] = size;
}

int Deletion(int* Heap, int* distance, int* Heap_Index, int size)
{
	int min_index = Heap[1], parent=1, child=2, temp = Heap[size--];//최소 값을 저장하고, 마지막 값을 저장한다. parent와 child를 초기화한다.

	Heap_Index[Heap[1]] = 0;
	while(child <= size){
		if((child < size) && distance[Heap[child]] > distance[Heap[child+1]])
			child++;//consider the odd index values.
		if(distance[temp] <= distance[Heap[child]]) break;//exit condition
		Heap[parent] = Heap[child];
		Heap_Index[Heap[parent]] = parent;
		parent = child;
		child = child * 2;//fix the values by the min heap algorithm
	}
	Heap[parent] = temp;//Last save
	Heap_Index[Heap[parent]] = parent;
	return min_index;
}

void Adjust(int* Heap, int* distance, int* Heap_Index, int vertex_W, int size){

	int search = 0;

	search = Heap_Index[vertex_W];//기준을 저장한다.
	while((search != 1) && (distance[vertex_W] < distance[Heap[search/2]])){
		Heap[search] = Heap[search/2];
		Heap_Index[Heap[search]] = search;
		search = search / 2;
	}//삽입하는 알고리즘과 동일하다.
	Heap[search] = vertex_W;
	Heap_Index[Heap[search]] = search;

}

int SPT_Dijkstra(		// return SPT cost
	int src,			// source vertex index
	int Vnum, vertex *V, int Enum, edge *E,		// Graph data(the same as in HW04, 05)
	int *P1, int *P2, int *P3, int *P4,			// scratch int  storage of size (Vnum+1)
	bool *P5,									// scratch bool storage of size (Vnum+1)
	int *Maxd1,		// The largest distance (output)
	int *Maxd3      // The third largest distance (output)
	){
		//Let P1 be distance array, P2 be Heap array, P3 be Heap_index array,
		//P4 be inc_Edge array, P5 be Found array.

		int total_cost=0; int i, vertex_U, vertex_W, edge_index, size, temp, max, max_index = 0;

		//First, initialize
		for(i=0;i<Vnum+1;i++){
			P1[i] = INT_MAX;
			P3[i] = 0;
			P4[i] = INT_MAX;
			P5[i] = false;
		}
		P5[src] = true; P1[src] = 0;

		//src로부터 인접한 vertex들에 대하여 distance값, 연결 edge 등을 저장한다.
		//f_hd로부터 탐색
		for(i=V[src].f_hd;i!=-1;i=E[i].fp){
			edge_index = i;
			vertex_U = E[i].vr;
			P1[vertex_U] = E[edge_index].cost;
			P4[vertex_U] = edge_index;
			total_cost += E[edge_index].cost;
			E[edge_index].flag = true;
		}
		//r_hd로부터 탐색
		for(i=V[src].r_hd;i!=-1;i=E[i].rp){
			edge_index = i;
			vertex_U = E[i].vf;
			P1[vertex_U] = E[edge_index].cost;
			P4[vertex_U] = edge_index;
			total_cost += E[edge_index].cost;
			E[edge_index].flag = true;
		}

		//sort by heap sorting
		for(i=0; i<Vnum; i++)
			Insertion(P2,P1,P3,i,i);
		size = Vnum;

		//make SPT by choosing Vertex
		for(i=0; i<Vnum-2; i++){
			while(size > 0){
				vertex_U = Deletion(P2,P1,P3,size--);
				if(P5[vertex_U] == false)
					break;
			}// Choose a vertex u such that found[u]=F and distance[u] is minimum
			P5[vertex_U] = true;

			//선택한 vertex U에 인접한 vertex들에대하여 distance값의 update를 진행한다.
			//f_hd로부터 탐색
			for(int j=V[vertex_U].f_hd; j != -1; j=E[j].fp){
				edge_index = j;
				vertex_W = E[edge_index].vr;
				if((P5[vertex_W] == false) && (P1[vertex_U] + E[edge_index].cost <= P1[vertex_W])){
					if(P1[vertex_W]!=INT_MAX){
						total_cost -= E[P4[vertex_W]].cost;
						E[P4[vertex_W]].flag = false;//더 짧은 경로가 존재하므로 기존 SPT에 추가돼있던 edge를 삭제한다.
					}
					P1[vertex_W] = P1[vertex_U] + E[edge_index].cost;
					P4[vertex_W] = edge_index;
					Adjust(P2,P1,P3,vertex_W,size);
					total_cost += E[P4[vertex_W]].cost;
					E[edge_index].flag = true;//힙을 수정하고 distance값을 갱신하고, SPT에 새로운 path를 저장한다.
				}
			}
			//r_hd로부터 탐색
			for(int j=V[vertex_U].r_hd; j != -1; j=E[j].rp){
				edge_index = j;
				vertex_W = E[edge_index].vf;
				if((P5[vertex_W] == false) && (P1[vertex_U] + E[edge_index].cost <= P1[vertex_W])){
					if(P1[vertex_W]!=INT_MAX){
						total_cost -= E[P4[vertex_W]].cost;
						E[P4[vertex_W]].flag = false;
					}
					P1[vertex_W] = P1[vertex_U] + E[edge_index].cost;
					P4[vertex_W] = edge_index;
					Adjust(P2,P1,P3,vertex_W,size);
					total_cost += E[P4[vertex_W]].cost;
					E[edge_index].flag = true;
				}
			}
		}
		//selection sort를 이용하여 Maxd1과 Maxd3값을 찾는다.
		for(i=0;i<Vnum-1;i++){
			max = P1[i];
			for(int j=i+1; j<Vnum; j++)
				if(max<P1[j]){
					max =P1[j];
					max_index=j;
				}
			temp = P1[i];
			P1[i] = P1[max_index];
			P1[max_index] = temp;
		}

		*Maxd1 = P1[0];
		*Maxd3 = P1[2];
		return total_cost;
}
