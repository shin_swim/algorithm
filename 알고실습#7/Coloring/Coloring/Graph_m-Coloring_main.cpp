#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int Graph_mColoring_top(
	int N,			// vertex의 개수
	int m,			// 허용된 칼라수
	int *Color,		// 배정된 칼라 값 (꼭 0, 1, 2, 등 순서대로 일 필요는 없음)
					// 즉, 2, 5, 8 등 연속이 아니어도 무방함.
	int *P1, int *P2, int *P3,	// 크기가 N인 임시 정수 배열
	char **Adj_M	// adjacency matrix (정수 1 또는 0이 저장)
);

void Allocate_Memory(char ***Adj_M, int **Color, int **P1, int **P2, int **P3, int N);
void Free_Memory(char ***Adj_M, int **Color, int **P1, int **P2, int **P3, int N);
void Error_Exit(char *s);

#define NONE	-1	// no color assigned (MUST use this value in your program for no color)

void main( void ) {
	int T, N, m, tmp;
	char **Adj_M;
	int *Color, *P1, *P2, *P3, max_color;
	clock_t start, finish;
	double  cmpt;

	// Input the number of test cases
	scanf("%d", &T);
	for ( int t = 0; t < T; t++ ) {
		// input N and m
		scanf("%d %d", &N, &m);
		Allocate_Memory(&Adj_M, &Color, &P1, &P2, &P3, N);
		// read adjacent matrix
		for ( int r = 0; r < N; r++ ) {
			for ( int c = 0; c < N; c++ ) {
				scanf("%d", &tmp);
				Adj_M[r][c] = (char)tmp;
			}
		}
		if ( t != 0 ) {
			printf("\n");
		}

		start = clock();
		max_color = Graph_mColoring_top(N, m, Color, P1, P2, P3, Adj_M);
		finish = clock();
		cmpt = ((double)(finish - start)) / (double)CLK_TCK;

		if ( max_color != 0 ) {
			printf("%d ", max_color);
			for (int v = 0; v < N; v++ ) {
				printf("%d ", Color[v]+1);
			}
			// Check the validity of coloring
			for ( int v = 0; v < N; v++ ) { 
				for ( int w = 0; w < N; w++ ) {
					if ( w == v ) {
						continue;
					}
					if ( Adj_M[v][w] == 1 ) {
						if ( Color[v] == Color[w] ) {
							printf("\nERROR: adjacent nodes %d and %d have the same color %d\n", v, w, Color[v]);
						}
					}
				}
			}
		}
		else {
			printf("0");
		}
		printf("\n%.2f sec", cmpt);
		Free_Memory(&Adj_M, &Color, &P1, &P2, &P3, N);

	}
}

void Allocate_Memory(char ***Adj_M, int **Color, int **P1, int **P2, int **P3, int N) {
	for ( int i = 0; i < N; i++ ) {
		*Adj_M = (char **)malloc(N * sizeof(char *));
		if ( *Adj_M == NULL ) {
			Error_Exit("Memory Allocation Error(Adj_M)");
		}
		for ( int j = 0; j < N; j++ ) {
			(*Adj_M)[j] = (char *)malloc(N * sizeof(char));
			if ( (*Adj_M)[j] == NULL ) {
				Error_Exit("Memory Allocation Error(Adj_M[j])");
			}
		}
	}
	*Color = (int *)malloc(N * sizeof(int));
	if ( *Color == NULL ) {
		Error_Exit("Memory Allocation Error(Vidx)");
	}
	*P1 = (int *)malloc(N * sizeof(int));
	if ( *P1 == NULL ) {
		Error_Exit("Memory Allocation Error(Degree)");
	}
	*P2 = (int *)malloc(N * sizeof(int));
	if ( *P2 == NULL ) {
		Error_Exit("Memory Allocation Error(Color)");
	}

	*P3 = (int *)malloc(N * sizeof(int));
	if ( *P3 == NULL ) {
		Error_Exit("Memory Allocation Error(Tmp)");
	}

}

void Free_Memory(char ***Adj_M, int **Color, int **P1, int **P2, int **P3, int N) {
	for ( int r = 0; r < N; r++ ) {
		free((*Adj_M)[r]);
	}
	free(*Adj_M);
	Adj_M = NULL;
	free(*Color);
	*Color = NULL;
	free(*P1);
	*P1 = NULL;
	free(*P2);
	*P2 = NULL;
	free(*P3);
	*P3 = NULL;
}

void Error_Exit(char *s) {
	printf("*s", s);
	exit(-1);
}