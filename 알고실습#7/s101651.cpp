void sort_vertex(char** Adj_M, int* P1, int* P2, int m, int N){
	int max_index, max_count;
	for(int i=0; i<N; i++){
		P1[i]=0;
		for(int j=0; j<N; j++)
			if(Adj_M[i][j] == 1)
				P1[i]++;
	}//Count adj numbers of each vertices.
	for(int i=0; i<N; i++){
		max_index = i; max_count = P1[i];
		for(int j=0; j<N; j++)
			if(P1[j]>max_count){
				max_count = P1[j];
				max_index = j;
			}
		P2[i] = max_index;
		P1[max_index] = -1;
	}//Sort the vertex index to P2 array from P1 array's data.
}

bool promising(char** Adj_M, int* P2, int* Color, int idx, int N ){
	bool chk=true;

	for(int i=0; i<N; i++)
		if(Adj_M[P2[idx]][i] == 1)
			if(Color[P2[idx]] == Color[i]){
				chk=false;				
				break;//if adj vertex has same color, it is not promising.
			}

	return chk;
}
void G_Coloring( char** Adj_M, int* Color, int* P2, int index,  bool* flag, int m, int N, int& color_cnt){
	int max=0;

	if(promising(Adj_M,P2, Color,index,N)){
		
		if(index==N-1){
			for(int i=0; i<N; i++)
				if(Color[i] > max) 
					max=Color[i];
			color_cnt = max;//find the max number of the colors.
			*flag=true;//if coloring is completed, change the flag true.
		}
		else{
			for(int i=1; i<=m; i++){
				Color[P2[index+1]]=i;//put color.
				G_Coloring(Adj_M,Color,P2, index+1,flag, m, N, color_cnt);//recursively call the function.
				if(*flag==true) return;//if it finished, return.
			}
		}

	}
}
int Graph_mColoring_top(
	int N,			// vertex의 개수
	int m,			// 허용된 칼라수
	int *Color,		// 배정된 칼라 값 (꼭 0, 1, 2, 등 순서대로 일 필요는 없음)
					// 즉, 2, 5, 8 등 연속이 아니어도 무방함.
	int *P1, int *P2, int *P3,	// 크기가 N인 임시 정수 배열
	char **Adj_M	// adjacency matrix (정수 1 또는 0이 저장)
){
	int result =0; bool flag = false;
	sort_vertex(Adj_M,P1,P2,m,N);
	Color[P2[0]] = 1;//initialize the most adj vertex's color to 1.
	G_Coloring(Adj_M, Color, P2, 0, &flag, m, N, result);
	if(flag == false)
		result = 0;//if it cannot be solved, result is 0.
	for(int i = 0; i<N; i++)
		Color[i]--;//Because the colors start with 1.
	return result;
}