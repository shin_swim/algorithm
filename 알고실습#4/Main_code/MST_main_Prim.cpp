#include "Graph_include.h"
#include <queue>

void Error_Exit ( char *s );
int  Tree_Check(int Vnum, vertex *V, int Enum, edge *E, int *visited);
void Read_Graph(int Vnum, vertex *V, int Enum, edge *E);
int  MST_Prim ( 
	int     Vnum,  // number of vertices
	vertex *V,     // vertex structure array (starting index 0)
	int     Enum,  // number of edges
	edge   *E,     // edge structure array (starting index 0)
	int    *P1,    // scratch storage of size (Vnum + 1)
	int    *P2     // scratch storage of size (Vnum + 1)
);

void main ( void ) {
	int Vnum, Enum, src;
	vertex *V;
	edge   *E;
	int    *PV1, *PV2;
	int    Tree_cost;
	int    Tnum; // # of test cases
	clock_t start, finish;
	double cmpt;

	scanf_s("%d", &Tnum);

	for (int t = 0; t < Tnum; t++ ) {
		scanf_s("%d %d %d", &Vnum, &Enum, &src);
		V = new vertex [Vnum];
		E = new edge [Enum];
		if ( V == NULL || E == NULL ) {
			Error_Exit("Memory Allocation Error");
		}
		Read_Graph(Vnum, V, Enum, E);

		PV1 = new int [Vnum+1]; PV2 = new int [Vnum+1];

		if ( PV1 == NULL || PV2 == NULL ) {
			Error_Exit("Memory Allocation Error");
		}
		// MST by Prim
		start = clock();
		Tree_cost = MST_Prim ( Vnum, V, Enum, E, PV1, PV2 );
		finish = clock();
		cmpt = ((double)(finish - start)) / (double)CLK_TCK;

        if (Tree_Check(Vnum, V, Enum, E, PV1) == 0) { // check if spanning tree
			if ( t != 0 ) printf("\n");
			printf("**T%d (Prim) (V = %d, E = %d, time = %.3f sec) ERROR: NOT A TREE!!", 
				t+1, Vnum, Enum, cmpt);
		}
		else {
			if ( t != 0 ) printf("\n");
			printf("**T%d (Prim) (V = %d, E = %d, time = %.3f sec) Tree Cost = %d", 
				t+1, Vnum, Enum, cmpt, Tree_cost);
		}

		delete PV1; delete PV2;
	}
}

void Read_Graph(int Vnum, vertex *V, int Enum, edge *E) {
	// read graph information
	int k;
	for (k = 0; k < Vnum; k++ ) { // init vertex array ptrs
		V[k].f_hd = V[k].r_hd = NONE;
	}
	for (k = 0; k < Enum; k++ ) { // init vertex array ptrs
		//E[k].fp = E[k].rp = NONE;
		E[k].flag = false;
		scanf_s("%d %d %d", &(E[k].vf), &(E[k].vr), &(E[k].cost));
		if ( V[E[k].vf].f_hd == NONE ) {
			E[k].fp = NONE;
		}
		else {
			E[k].fp = V[E[k].vf].f_hd;
		}
		V[E[k].vf].f_hd = k;

		if ( V[E[k].vr].r_hd == NONE ) {
			E[k].rp = NONE;
		}
		else {
			E[k].rp = V[E[k].vr].r_hd;
		}
		V[E[k].vr].r_hd = k;
	}
}

#define PARENT_OF_ROOT  -100

int Tree_Check(int Vnum, vertex *V, int Enum, edge *E, int *visited) {
	
	int u = 0, ep, w;
	std::queue<int> q;
	for ( int i = 0; i < Enum; i++ ) {
		if ( E[i].flag == true ) {
			++u;
		}
	}
	if ( u != (Vnum-1) ) {
		return 0; // it is not a spanning tree
	}
	for (int i=0; i < Vnum; i++ )
		visited[i] = NONE;
	u = 0;
	visited[u] = PARENT_OF_ROOT;
 	do {
		// check adjacent list at v0 and set Costi_Ni
		for ( ep = V[u].f_hd; ep != NONE; ep = E[ep].fp ) { // forward
			if ( E[ep].flag == false ) {
				continue;
			}
			w = E[ep].vr;
			if ( visited[w] == NONE ) {
				visited[w] = u;
				q.push(w);
			}
			else if ( visited[u] != w ) { // w is visited and w is not the parent of v,
										  // implies that G has a cycle
				while ( !q.empty() ) {
					q.pop();
				}
				return 0;
			}
		}
		for ( ep = V[u].r_hd; ep != NONE; ep = E[ep].rp ) { // backward
			if ( E[ep].flag == false ) {
				continue;
			}
			w = E[ep].vf;
			if ( visited[w] == NONE ) {
				visited[w] = u;
				q.push(w);
			}
			else if ( visited[u] != w ) { // w is visited and w is not the parent of v,
										  // implies that G has a cycle
				while ( !q.empty() ) {
					q.pop();
				}
				return 0;
			}
		}
		if ( q.empty() ) {
			for (int i=0; i < Vnum; i++ )
				if (visited[i] == NONE)
					return 0;			// the graph is not connected
			return 1;
		}
		u = q.front();
		q.pop();	
	} while ( 1 );
}

void Error_Exit ( char *s ) {
  printf("%s\n", s);
  exit(-1);
}