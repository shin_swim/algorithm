#include<stdlib.h>
#include"..\ExternDoc.h"
#include "..\UI\Zoom.h"
#include "..\UI\MsgView.h"
#include "..\Graphics\DrawFunc.h"
#include "..\Example\Example.h"


#define ERROR_NUMBER -1

//functino prototype
static void drawDirect(CDC* pDC);
static void drawBuffered();

//Start of user code
#include <Float.h>
typedef struct ptr_list ptr_L;
typedef struct vertex VTX;
typedef struct edge EDGE;

struct ptr_list{//linked list definition
	int i;
	ptr_L *p;
};

struct vertex{
	int x, y;//x,y position of the vertex
	ptr_L *p;//incident edge array index list
};

struct edge{
	int v1, v2;//adjacent vertex array indices
	int cost;//distance between the two aertices
};

VTX *V = NULL;//Vertex array pointer
EDGE *E = NULL;//Edge array pointer
int V_Num = 0, E_Num = 0;//vertex and edge num
ptr_L *ptr_L_pool = NULL;//ptr_L storage pool
int ptr_L_cnt = 0;//number of ptr_L used
int Total_Used_Memory = 0;

//ptr_L management routine
void pushQ_ptr_L(ptr_L **Q, int i );
int popQ_ptr_L(ptr_L **Q);
ptr_L *addFIFO_ptr_L(ptr_L**Q, ptr_L **Q_end, int sym);
int getFIFO_ptr_L(ptr_L **Q, ptr_L **Q_end);
ptr_L *Alloc_ptr_L(void);
void Free_ptr_L(ptr_L *ptr);
void Free_ptr_L_pool(void);
void Error_Exit(char *s);  //error handling routine
							//usefull in debugging
/*************************************************
* function : bool readFile(const char* filename)
* 
* argument : const char* filename - filename to be opened
* return : true if success, otherwise false
* remark : After read data file, physical view must be set,
             ohterwise drawing will not be executed correctly.
             The window will be invalidated after readFile()
                 return true.
*************************************************/


bool readFile(const char* filename){
        //start of the user code
        char str[MAX_PATH], vtx1[30], vtx2[30];
        FILE *fp;
        bool flag;
        char vflag;
        int v, nvtx1, nvtx2, x1, y1, eid, i;
        int total_cost = 0;


        fp = fopen(filename, "r");
        if(fp == NULL){
                flag = false;
                return flag;
        }
        flag = true;
        // Count V_Num and E_Num;
        vflag = 0;
        while( !feof(fp) ){
                for(i=0; i < MAX_PATH; i++){
                        str[i] = 0;
                }
        fgets(str, 100, fp);
        if(str[0] == 0){
                continue; //skip blank lines
        }
                if(vflag == 1 && str[0] != '#')
                        ++V_Num;
                else if(vflag == 2 && str[0] != '#')
                        ++E_Num;
        
        if(str[0] == '#' && str[1]=='v')
                vflag = 1;
        else if(str[0] == '#' && str[1] == 'e')
                vflag = 2;
}
//allocate the vertex and the edge arrays
V = (VTX *)malloc(V_Num * sizeof(VTX) );
E = (EDGE *)malloc(E_Num*sizeof(EDGE) );
if(V == NULL || E == NULL)
        Error_Exit("Malloc Error");
Total_Used_Memory += V_Num * sizeof(VTX) + E_Num*sizeof(EDGE);
//initialize the incident list of each vertex




for(v = 0; v<V_Num; v++)
        V[v].p = NULL;
//construct the incident list
rewind (fp);
vflag = 0;
g_xMin = g_yMin = DBL_MAX; g_xMax = g_yMax = DBL_MIN;
eid = 0;
while(!feof(fp)){
        //this loop is not well written
        //there must be a better way of coding
        for(i = 0; i < MAX_PATH ; i++)
                str[i] = 0;
        for(i = 0; i < 30; i++){
                vtx1[i] = vtx2[i] = 0;
        }
        fgets(str, 100, fp);
        if(str[0] == 0){
                continue; //skip if blank
        }
        if(vflag ==1 && str[0] != '#'){
                sscanf(str, "%s %d %d", vtx1, &x1, &y1);
                vtx1[0] = ' ';
                sscanf(vtx1, "%d", &nvtx1);
                V[nvtx1].x = x1; V[nvtx1].y = y1;
                if(g_xMin > (double)x1)
                        g_xMin = (double)x1;
                if(g_xMax < (double)x1)
                        g_xMax = (double)x1;
                if(g_yMin > (double)y1)
                        g_yMin = (double)y1;
                if(g_yMax < (double)y1)
                        g_yMax = (double)y1;
}
        else if(vflag == 2 && str[0] != '#'){
                sscanf(str, "%s %s", vtx1, vtx2);
                if(vtx1[0] != 'v' || vtx2[0] != 'v'){
                        --E_Num;
                continue;
                }
        vtx1[0] = vtx2[0] = ' ';
                sscanf(vtx1, "%d", &nvtx1);
        sscanf(vtx2, "%d", &nvtx2);
        E[eid].v1 = nvtx1; E[eid].v2 = nvtx2;
        E[eid].cost = V[nvtx1].x - V[nvtx2].x;
        if(E[eid].cost < 0)
                E[eid].cost = -E[eid].cost;
        if((V[nvtx1].y - V[nvtx2].y) < 0)
                E[eid].cost += V[nvtx2].y - V[nvtx1].y;
        else
                E[eid].cost += V[nvtx1].y - V[nvtx2].y;
        total_cost += E[eid].cost;
        pushQ_ptr_L(&(V[nvtx1].p), eid);
        pushQ_ptr_L(&(V[nvtx2].p), eid);
        eid++;
}
if(str[0] == '#' && str[1] == 'v')
        vflag = 1;
else if(str[0] == '#' && str[1] == 'e')
        vflag = 2;
}
//if success
if(flag){
        //Physical view must be set before drawing
        //min, max value of x, y may be obtained from the user data
        setWindow(g_xMin, g_yMin, g_xMax, g_yMax, 0);
        
        //show message
        sprintf(str, "File %s is loaded", filename);
        showMessage(str);
        sprintf(str, "Total Edge Cost = %d", total_cost);
        showMessage(str);
}
return flag;
//end of the user code
}
/*******************************************************************
 * function : void freeMemory()
 *
 * remark   : Free allocated user storage
 *******************************************************************/
void freeMemory(){
	// start of the user code
	int i;
	
	if ( V != NULL ) {
		for ( i = 0; i < V_Num; i++ )
			while ( V[i].p != NULL)
				popQ_ptr_L(&(V[i].p));
		free(V);
		V = NULL;
		V_Num = 0;
		Total_Used_Memory -= V_Num * sizeof(VTX);
	}
	if ( E != NULL ) {
		free(E);
		E = NULL;
		E_Num = 0;
		Total_Used_Memory -= E_Num * sizeof(EDGE);
	}
	Free_ptr_L_pool ( );
	// check if ptr_L operation is correct
	if ( ptr_L_cnt != 0 )
		Error_Exit("Storage does not cleared");
	// end of user code
}

/**********************************************************************
 * function : bool writeFile(const char* filename)
 *
 * argument : const char* filename - file name to be written
 * return   : true if succes, otherwise false
 * remark   : Save user data to a file
 ***********************************************************************/
bool writeFile(const char* filename) {
	// start of the user code
	bool flag;

//  setModifiedFlag(false); // may be useful later

	flag = 0;
	return flag;
	// end of user code
}

/*************************************************************************
 * function : void go()
 *
 * remark   : "GO" menu handler function
 **************************************************************************/
void go( void ) {
	// start of user code
		// Nothing to write currently
	// end of user code
}
/**********************************************************
 * function : void drawMain(CDC* pDC)
 *
 * argument : CDC* pDC - device context object pointer
 * remark   : Main drawing function. Caaled by CMFC_MainView::OnDraw()
 ***********************************************************/
void drawMain(CDC* pDC) {
// if direct drawing is defined
#if defined(GRAPHICS_DIRECT)
	drawDirect(pDC) ;
// if buffered drawing is defined
#elif defined(GRAPHICS_BUFFERED)
	drawBuffered();
#endif
}

/************************************************************
 * function : static void drawDirect(CDC* pDC)
 *
 * argument : CDC* pDC - device context object pointer
 * remark   : Direct drawing routines here.
 *************************************************************/
static void drawDirect(CDC* pDC) {
	// begin of user code
		//Nothing to write currently
	// end of user code
}

/**************************************************************
 * function : static void drawBuffered()
 *
 * argument : CDC* pDC - device context object pointer
 * remark   : Buffered drawing routines here.
 ***************************************************************/
static void drawBuffered() {
	// start of the user code
	double xmin, xmax, ymin, ymax, v_size = 0.4;
	int lineWidth = 1, i;
	// draw edges
	for ( i = 0; i < E_Num; i++ ) {
		xmin = V[E[i].v1].x;
		ymin = V[E[i].v1].y;
		xmax = V[E[i].v2].x;
		ymax = V[E[i].v2].y;
		DrawLine_I(xmin, ymin, xmax, ymax, lineWidth, RGB(0,255,0));
	}
	//draw vertices
	for ( i = 0; i < V_Num; i++ ) {
		xmin = (double)V[i].x - v_size;
		xmax = (double)V[i].x + v_size;
		ymin = (double)V[i].y - v_size;
		ymax = (double)V[i].y + v_size;
		DrawSolidBox_I(xmin, ymin, xmax, ymax, lineWidth, RGB(20,20,20), RGB(0,0,255));
	}
	// end of the user code
}
// ptr_L linked list management routines
void pushQ_ptr_L ( ptr_L **Q, int i ) { // for LIFO
	ptr_L *L;

	L    = Alloc_ptr_L();
	L->i = i;

	L->p = *Q;
	*Q   =  L;

	return;
}

int popQ_ptr_L ( ptr_L **Q ) { // for LIFO
	int i;
	ptr_L   *tQ;

	i   = (*Q) ->i;
	tQ  = *Q;
	if ( (*Q)->p == NULL )
		*Q = NULL;
	else
		*Q = (*Q)->p;

	Free_ptr_L( tQ );
	return ( i );
}

ptr_L *addFIFO_ptr_L ( ptr_L **Q, ptr_L **Q_end, int sym ) { // for FIFO
	ptr_L *L;

	L    = Alloc_ptr_L();
	L->i = sym;
	if ( *Q == NULL )
		*Q = L;
	else
		(*Q_end)->p = L;
	*Q_end = L;
	return ( L );
}

int getFIFO_ptr_L ( ptr_L **Q, ptr_L **Q_end ) { // for FIFO
	int sym;
	ptr_L *tQ;

	sym = (*Q)->i;
	tQ  = *Q;
	if ( (*Q)->p == NULL ) {
		*Q = *Q_end = NULL;
	}
	else
		*Q = (*Q)->p;
	Free_ptr_L( tQ );
	return ( sym );
}
ptr_L *Alloc_ptr_L(void) {
        ptr_L *ptr;
 
        if( ptr_L_pool == NULL) {
                ptr = (ptr_L *)malloc(sizeof(ptr_L));
                if(ptr == NULL)
                        Error_Exit("No Memory in Alloc_ptr_L");
                Total_Used_Memory += sizeof(ptr_L);
                ptr -> i = ERROR_NUMBER;
        }
        else{
                ptr = ptr_L_pool;
                ptr_L_pool = ptr -> p;
        }
        ptr->p = NULL;
        ++ptr_L_cnt;
        return(ptr);
}


void Free_ptr_L(ptr_L *ptr){
        if(ptr -> i == ERROR_NUMBER) // for debugging only
                Error_Exit("This storage is already freed(Free_ptr_L).");
        ptr -> i = ERROR_NUMBER;        
        ptr -> p = ptr_L_pool;
        ptr_L_pool = ptr;
        --ptr_L_cnt;
}


void Free_ptr_L_pool (void) {
        ptr_L *p;


        p = ptr_L_pool;
        while(p != NULL){
                ptr_L_pool = p -> p;
                free(p);
                p = ptr_L_pool;
                Total_Used_Memory -= sizeof(ptr_L);
        }
        if(ptr_L_cnt != 0) //just for debugging
                Error_Exit ("Non-zero ptr_L_cnt after deallocate ptr_L_pool.");
        ptr_L_pool = NULL;
}


void Error_Exit (char *s){ //for debugging only
        showMessage(s);
        exit(-1);        //you may break here for possible error
}