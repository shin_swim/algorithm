#include<math.h>
#include<float.h>

double min ( double x, double y){
	return ( x <= y )? x : y;
}//compare two values and return the small value.

double dist( double* X, double* Y, unsigned int P1, unsigned int P2){
	return sqrt((X[P1]-X[P2])*(X[P1]-X[P2]) + (Y[P1] - Y[P2])*(Y[P1] - Y[P2]));
}//calculate the distance between two points and return result.

double findClosest(double *X, double *Y,unsigned int *P2,unsigned int *pt1, unsigned int *pt2, unsigned int n, double d)
{
    double min = d;  // Initialize the minimum distance as d

    for (  int i = 0; i < n; ++i)
        for (  int j = i+1; j < n && (X[P2[j]] - X[P2[i]]) < min; ++j)//x좌표상 거리 min내에있는 점들에대해 거리를 비교한다.
            if (dist(X,Y,P2[i],P2[j]) < min){
				*pt1=P2[i];
				*pt2=P2[j];//더 작은 거리를 갖는 두 점을 저장한다.
				min = dist(X,Y,P2[i],P2[j]);//가장 작은 거리를 저장한다.
			}
 
    return min;
}//

double Brute_Force( double* X, double* Y, unsigned int* P, unsigned int L, unsigned int R, unsigned int *pt1, unsigned int *pt2){
	double min = DBL_MAX;
	*pt1 = L; *pt2 = R;
	for(int i=L;i<R;i++)
		for(int j=i+1;j<=R;j++)
			if(dist(X,Y,P[i],P[j]) < min){
				*pt1 = P[i];
				*pt2 = P[j];
				min = dist(X,Y,P[i],P[j]);
			}
	return min;
}//L과 R사이에 존재하는 모든 점들에 대해서 거리를 구하고, 가장 작은 거리와 그 두 점을 저장한다.

double Closest_Pair_DC(
	unsigned int L,    unsigned int R,		// left and right indices
	unsigned int *pt1, unsigned int *pt2,   // min dist points inices
    double *X, double *Y,					// input points position array
	unsigned int *P1, unsigned int *P2, unsigned int *P3, // temp index arrays
	unsigned int THR	// threshold value
	){
		if(L==R){
			*pt1 = L;
			*pt2 = L;
			return 0;
		}

		if((R-L) <= THR)
			return Brute_Force(X,Y,P1,L,R,pt1,pt2);


		int mid = L + (R-L)/2; int j=0;

		double dl = Closest_Pair_DC(L,mid,pt1,pt2,X,Y,P1,P2,P3,THR);
		
		unsigned int dlp = *pt1;
		unsigned int dlq = *pt2;
		//분할 후 왼쪽 집합에서 거리와 두 점을 찾은 뒤, 저장한다.
		double dr = Closest_Pair_DC(mid,R,pt1,pt2,X,Y,P1,P2,P3,THR);

		unsigned int drp = *pt1;
		unsigned int drq = *pt2;
		//분할 후 오른쪽 집합에서 거리와 두 점을 찾은 뒤, 저장한다.
		double d = min(dl,dr);
		//두 거리중 더 작은 거리를 d에 저장한다.
		if(d == dr){
			*pt1=drp;
			*pt2=drq;
		}
		else {
			*pt1=dlp;
			*pt2=dlq;
		}
		// 각각 구한 두 점 중 더 작은 거리를 갖는 두 점을 저장한다.
		for( int i = L; i <= R; i++)
			if( abs(X[P1[i]] - X[P1[mid]]) < d){
				P2[j] = P1[i]; j++;
			}
		//절대값으로서 d보다 작은 거리내에 존재하는 x좌표들에대해 그 정보를 P2에 저장한다.
		return min(d, findClosest(X,Y,P2,pt1,pt2,j,d));
		/*앞서 구한 d와 d보다 작은 거리내에 존재하는 x좌표 점들에대한 최소 거리를 구한 후,
		둘중 더 작은 값을 최소 거리로 return 한다.*/
}

void partition ( double *X, unsigned int *P, int low, int high, int& pivotpoint ) {
	int i,j;
	double pivotitem; double temp;
	pivotitem = X[P[low]]; j = low;
	for( i = low + 1; i<=high; i++)
		if( X[P[i]] < pivotitem){
			j++;
			temp = P[i];
			P[i] = P[j];
			P[j] = temp;
		}
		pivotpoint = j;
		temp = P[low];
		P[low] = P[pivotpoint];
		P[pivotpoint] = temp;
}//pivotpoint를 기준으로 더 작은 값을 왼쪽, 큰 값을 오른쪽으로 이동시키고 그 중간에 pivotpoint값을 옮겨주는 기능을 수행한다.

void quicksort ( double *X, unsigned int *P, int low, int high ){
	int pivot_point;
	if ( high > low ){
		partition ( X, P, low, high, pivot_point);
		quicksort ( X, P, low, pivot_point - 1 );
		quicksort ( X, P, pivot_point + 1, high );
	}
}//partition을 수행하며 작은 값, 큰 값을 분할시키고 recursive하게 분할된 곳에서 재수행함으로써 모든 값들을 정렬시킨다.
void   Sort_by_X ( double *X, unsigned int *P, unsigned int N ){
	quicksort(X, P , 0, N-1);
}//x좌표값을 기준으로 정렬을 수행하는 함수를 호출한다.

